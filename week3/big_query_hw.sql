CREATE OR REPLACE EXTERNAL TABLE `wide-journey-375717.zoomcamp_week4.fhv_tripdata`
OPTIONS (
  format = 'CSV',
  uris = ['gs://prefect-de-zoomcamp-arcry/data/fhv/fhv_tripdata_2019-*.csv.gz']
);

-- Q1 What is the count for fhv vehicle records for year 2019?

SELECT count(*) FROM `wide-journey-375717.zoomcamp_week4.fhv_tripdata`;

-- Q2 Write a query to count the distinct number of affiliated_base_number for the entire dataset on both the tables.
-- What is the estimated amount of data that will be read when this query is executed on the External Table and the Table?

SELECT COUNT(DISTINCT(affiliated_base_number)) FROM `wide-journey-375717.zoomcamp_week4.fhv_tripdata`;

CREATE OR REPLACE TABLE `wide-journey-375717.zoomcamp_week4.fhv_nonpartitioned_tripdata`
AS SELECT * FROM `wide-journey-375717.zoomcamp_week4.fhv_tripdata`;

SELECT COUNT(DISTINCT(affiliated_base_number)) FROM `wide-journey-375717.zoomcamp_week4.fhv_nonpartitioned_tripdata`;

-- Q3 How many records have both a blank (null) PUlocationID and DOlocationID in the entire dataset?

SELECT count(*)
FROM `wide-journey-375717.zoomcamp_week4.fhv_nonpartitioned_tripdata`
WHERE PUlocationID is NULL and DOlocationID is NULL;

-- Q4 What is the best strategy to optimize the table if query always filter by pickup_datetime and order by affiliated_base_number?

CREATE OR REPLACE TABLE `wide-journey-375717.zoomcamp_week4.fhv_partitioned_tripdata_clx2`
CLUSTER BY affiliated_base_number,  pickup_datetime AS (
  SELECT * FROM `wide-journey-375717.zoomcamp_week4.fhv_tripdata`
);

CREATE OR REPLACE TABLE `wide-journey-375717.zoomcamp_week4.fhv_partitioned_tripdata`
PARTITION BY DATE(pickup_datetime)
CLUSTER BY affiliated_base_number AS (
  SELECT * FROM `wide-journey-375717.zoomcamp_week4.fhv_tripdata`
);

CREATE OR REPLACE TABLE `wide-journey-375717.zoomcamp_week4.fhv_partitioned_tripdata_cldt_pt`
PARTITION BY RANGE_BUCKET(affiliated_base_number)
CLUSTER BY pickup_datetime AS (
  SELECT * FROM `wide-journey-375717.zoomcamp_week4.fhv_tripdata`
);


SELECT *
FROM `wide-journey-375717.zoomcamp_week4.fhv_partitioned_tripdata_clx2`
WHERE pickup_datetime BETWEEN '2019-01-01' AND '2019-03-31'
ORDER BY affiliated_base_number;

SELECT *
FROM `wide-journey-375717.zoomcamp_week4.fhv_partitioned_tripdata_ptdt_cl`
WHERE pickup_datetime BETWEEN '2019-01-01' AND '2019-03-31'
ORDER BY affiliated_base_number;

--Q5 Implement the optimized solution you chose for question 4. Write a query to retrieve the distinct affiliated_base_number between pickup_datetime 2019/03/01 and 2019/03/31 (inclusive).
-- Use the BQ table you created ea/home/arcry/work/samplingrlier in your from clause and note the estimated bytes. Now change the table in the from clause to the partitioned table you created for question 4 and note the estimated bytes processed. What are these values? Choose the answer which most closely matches.

CREATE OR REPLACE TABLE `wide-journey-375717.zoomcamp_week4.fhv_partitioned_tripdata`
PARTITION BY DATE(pickup_datetime) AS (
  SELECT * FROM `wide-journey-375717.zoomcamp_week4.fhv_tripdata`
);

SELECT COUNT(DISTINCT(affiliated_base_number))
FROM `wide-journey-375717.zoomcamp_week4.fhv_partitioned_tripdata_ptdt_cl`
WHERE pickup_datetime BETWEEN '2019-03-01' AND '2019-03-31';

SELECT COUNT(DISTINCT(affiliated_base_number))
FROM `wide-journey-375717.zoomcamp_week4.fhv_nonpartitioned_tripdata`
WHERE pickup_datetime BETWEEN '2019-03-01' AND '2019-03-31';

