#!/usr/bin/env python
# coding: utf-8

import pandas as pd
from time import time
from sqlalchemy import create_engine
import pyarrow.parquet as pq
import click
import os
from tqdm import tqdm


@click.command()
@click.option('--user', type=click.STRING)
@click.option('--password', type=click.STRING)
@click.option('--host', type=click.STRING)
@click.option('--port', type=click.STRING)
@click.option('--db', type=click.STRING)
@click.option('--table_name', type=click.STRING)
@click.option('--url', type=click.STRING)
def main(
        user,
        password,
        host,
        port,
        db,
        table_name,
        url
):
    parquet_name = 'external.pqt'
    os.system(f'wget {url} -O {parquet_name}')
    engine = create_engine(f'postgresql://{user}:{password}@{host}:{port}/{db}')
    engine.connect()

    parquet_file = pq.ParquetFile(parquet_name)

    # create schema in DB
    next(
        parquet_file.iter_batches(batch_size=1)
    ).to_pandas(
    ).head(
        0
    ).to_sql(
        name=table_name, con=engine, if_exists='replace'
    )

    for batch in tqdm(parquet_file.iter_batches(batch_size=50000)):
        start_time = time()
        print('Load another chunk')
        batch_df = batch.to_pandas()
        batch_df.to_sql(name=table_name, con=engine, if_exists='append')
        end_time = time()
        print('Chunk inserted for %.3f seconds' % (end_time - start_time))

    print('Data uploaded')


if __name__ == '__main__':
    main()
